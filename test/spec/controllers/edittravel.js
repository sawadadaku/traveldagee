'use strict';

describe('Controller: EdittravelCtrl', function () {

  // load the controller's module
  beforeEach(module('mageetravelApp'));

  var EdittravelCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EdittravelCtrl = $controller('EdittravelCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EdittravelCtrl.awesomeThings.length).toBe(3);
  });
});
