'use strict';

describe('Controller: AddProjectaddCtrl', function () {

  // load the controller's module
  beforeEach(module('mageetravelApp'));

  var AddProjectaddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddProjectaddCtrl = $controller('AddProjectaddCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AddProjectaddCtrl.awesomeThings.length).toBe(3);
  });
});
