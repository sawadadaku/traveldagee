'use strict';

describe('Controller: AddProjectCtrl', function () {

  // load the controller's module
  beforeEach(module('mageetravelApp'));

  var AddProjectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddProjectCtrl = $controller('AddProjectCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AddProjectCtrl.awesomeThings.length).toBe(3);
  });
});
