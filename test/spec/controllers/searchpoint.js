'use strict';

describe('Controller: SearchpointCtrl', function () {

  // load the controller's module
  beforeEach(module('mageetravelApp'));

  var SearchpointCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SearchpointCtrl = $controller('SearchpointCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SearchpointCtrl.awesomeThings.length).toBe(3);
  });
});
