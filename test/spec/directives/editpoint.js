'use strict';

describe('Directive: editpoint', function () {

  // load the directive's module
  beforeEach(module('mageetravelApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<editpoint></editpoint>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the editpoint directive');
  }));
});
