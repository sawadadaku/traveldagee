'use strict';

describe('Filter: fakedata', function () {

  // load the filter's module
  beforeEach(module('mageetravelApp'));

  // initialize a new instance of the filter before each test
  var fakedata;
  beforeEach(inject(function ($filter) {
    fakedata = $filter('fakedata');
  }));

  it('should return the input prefixed with "fakedata filter:"', function () {
    var text = 'angularjs';
    expect(fakedata(text)).toBe('fakedata filter: ' + text);
  });

});
