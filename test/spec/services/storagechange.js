'use strict';

describe('Service: storageChange', function () {

  // load the service's module
  beforeEach(module('mageetravelApp'));

  // instantiate service
  var storageChange;
  beforeEach(inject(function (_storageChange_) {
    storageChange = _storageChange_;
  }));

  it('should do something', function () {
    expect(!!storageChange).toBe(true);
  });

});
