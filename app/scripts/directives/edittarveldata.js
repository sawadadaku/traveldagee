'use strict';

/**
 * @ngdoc directive
 * @name mageetravelApp.directive:EditTarvelData
 * @description
 * # EditTarvelData
 */
angular.module('mageetravelApp')
  .directive('editTarvelData', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the EditTarvelData directive');
      }
    };
  });
