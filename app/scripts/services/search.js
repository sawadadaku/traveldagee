'use strict';

/**
 * @ngdoc service
 * @name mageetravelApp.search
 * @description
 * # search
 * Factory in the mageetravelApp.
 */
angular.module('mageetravelApp')
  .factory('search', function () {
    // Service logic
    // ...
    var UIDvalue;
    var project=[];
    var setUID=function(data){
      UIDvalue=data.uid
      project=data;
      // console.log(UIDvalue);
    }
    var getUID=function(){
      return project
    }

    // Public API here
    return {
      setUID: setUID,
      getUID: getUID
    };
  });
