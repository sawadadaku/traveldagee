'use strict';

/**
 * @ngdoc service
 * @name mageetravelApp.storageChange
 * @description
 * # storageChange
 * Factory in the mageetravelApp.
 */
angular.module('mageetravelApp')
  .factory('storageChange', function () {
    // Service logic
    // ...
    var toSave=function(name,data){
      var savedata=localStorage[name] = JSON.stringify(data);
      return savedata;
    }
    var toGet=function(name){
      var getdata=JSON.parse(localStorage[name])
      return getdata;
    }

    // Public API here
    return {
      toSave:toSave,
      toGet:toGet
    };
  });
