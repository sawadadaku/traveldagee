'use strict';

/**
 * @ngdoc overview
 * @name mageetravelApp
 * @description
 * # mageetravelApp
 *
 * Main module of the application.
 */
angular
  .module('mageetravelApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'ui.tree'
  ])
  .config(function ($stateProvider,$routeProvider) {
            $stateProvider.state("index", {
                url: "/",
                controller: "MainCtrl",
                controllerAs: "main",
                templateUrl: "views/main.html"
            }).state("about", {
                url: "/about",
                controller: "AboutCtrl",
                controllerAs: "about",
                templateUrl: "views/about.html"
            }).state("edit", {
                url: "/edit",
                controller: "EdittravelCtrl",
                controllerAs: "etc",
                templateUrl: "views/edittravel.html"
            }).state("search", {
                url: "/search",
                controller: "SearchpointCtrl",
                controllerAs: "spc",
                templateUrl: "views/search.html"
            });
  });
