'use strict';

/**
 * @ngdoc function
 * @name mageetravelApp.controller:SearchpointCtrl
 * @description
 * # SearchpointCtrl
 * Controller of the mageetravelApp
 */
angular.module('mageetravelApp')
  .controller('SearchpointCtrl', function (storageChange,$scope,search,$state) {
    var vm=this;
    vm.pointinfo="views/page/pointinfo.html"
    
// ======製造假資料開始======
    vm.default=[{
        	id:1,
        	title:'第一筆假地點',
        	text:'想新增假景點的話，就在搜尋欄輸入你喜歡的字，再點擊右上角的按鈕 ※不輸入直接按也可以',
        	fav:0,
    		img:'http://placekitten.com/200/300'
        }
    ]

    if (localStorage['result'] == null) {
        storageChange.toSave('result', vm.default);
    }
    vm.result = storageChange.toGet('result');
    vm.add=function(){
    	var x=($scope.search||$scope.search=='')? $scope.search:'';
    	var num=vm.result.length+1;
    	var fakename=(x=='')? '景點'+num : x.title;
    	var tmp={
    		id:num,
    		title:fakename,
    		text: fakename+'是個好地方，有aa、BB、cc以及許多的DDD，適合大家去放鬆心情，解放壓力的好去處。',
    		fav:0,
    		img:'http://placekitten.com/g/400/240'
    	}
    	vm.result.push(tmp);
    	storageChange.toSave('result', vm.result);
    	$('.search').val('');
    }
	vm.info=function(data,index){
		vm.show=1;
		vm.selected=index;
		$scope.info={infotitle:data.title,context:data.text,worktime:'每週日08:00',phone:'(02)27338118',address:'台北市',web:'http://www.magee.tw',fav:data.fav};
	}
// ======製造假資料結束======
 

	vm.favadd=function(data){
		$scope.info.fav=!data;
		vm.result[vm.selected].fav=$scope.info.fav;
		storageChange.toSave('result', vm.result);
	}

    // ======景點加入行程=========
	vm.addpoint=function(data){
        if(search.getUID().length == 0){
            vm.errortext=!vm.errortext;
        }
        else{
            vm.getpro = search.getUID();
            vm.daylist = storageChange.toGet(vm.getpro.uid);
            vm.daysnum = vm.daylist.length;
            vm.isOpen=!vm.isOpen;
            vm.newpoint=data;
            vm.errortext=0;
            // console.log(vm.daysnum);
        }        
	}
    vm.pointinto = function(data) {
        // console.log(data);
        var targetday = data;
        var newname = vm.newpoint;
        var nowpoint = vm.daylist[targetday].nodes;
        var addItem = {
            id: nowpoint.length * 10 + 1,
            title: newname,
            pointkind: '',
            stop_hour: 0,
            stop_minute: 0
        };
        nowpoint.push(addItem);
        alert("已經在【" + "Day" + (targetday+1) + "】新增了1個【" + newname + "】景點");
        storageChange.toSave(vm.getpro.uid, vm.daylist);
        $state.go('edit')
    }
  });
