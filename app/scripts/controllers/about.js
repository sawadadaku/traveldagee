'use strict';

/**
 * @ngdoc function
 * @name mageetravelApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mageetravelApp
 */
angular.module('mageetravelApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
