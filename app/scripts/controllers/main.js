'use strict';

/**
 * @ngdoc function
 * @name mageetravelApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mageetravelApp
 */
angular.module('mageetravelApp')
  .controller('MainCtrl', function ($uibModal,storageChange,search) {
    var vm=this;
    // console.log(storageChange.toGet('projectlist'));
    vm.default=[
        {
        	uid:'PT170609001',
        	title:'我是標題',
        	dateStart:'2017-03-01',dateEnd:'2017-03-02',
        	stateType:1,
        	bgimg:'bg1',
        	version:1
        },{
        	uid:'PT170609002',
        	title:'沖繩',
        	dateStart:'2017-01-01',
        	dateEnd:'2017-01-02',
        	stateType:2,
        	bgimg:'bg2',
        	version:2
        }
    ]
    if(localStorage.projectlist==null){
    	var result=storageChange.toSave('projectlist',vm.default);
    }
    vm.delhide=function(data){
    	data.project.delhide=1;
    	var result=storageChange.toSave('projectlist',vm.default);
    }
    vm.sref=function(data){
    	search.setUID(data);
    }
    vm.projectlist=storageChange.toGet('projectlist');
    vm.addopen = function(){
    	var open=$uibModal.open({
	    	ariaLabelledBy: 'modal-title',
	    	ariaDescribedBy: 'modal-body',
	    	templateUrl: 'views/add/project.html',
	    	controller: 'AddProjectCtrl',
	    	controllerAs: 'apc',
	    	size: 'md',
	    	backdrop:'static',
	    	resolve: {

	    	}
	    });
	    open.result.then(function(data) {
            vm.projectlist.push(data);
            var result=storageChange.toSave('projectlist',vm.projectlist);
            // console.log(storageChange.toGet('projectlist'));
        });
    }
  });

