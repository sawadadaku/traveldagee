'use strict';

/**
 * @ngdoc function
 * @name mageetravelApp.controller:AddProjectCtrl
 * @description
 * # AddProjectCtrl
 * Controller of the mageetravelApp
 */
angular.module('mageetravelApp')
  .controller('AddProjectCtrl', function ($uibModalInstance) {
    var vm=this;
    var now=new Date();
    var n=(Math.floor(Math.random() * 50)%10+1);
    vm.uid="PT"+now.getFullYear().toString().slice(-2)+((now.getMonth() < 10) ? '0' : '')+(now.getMonth()+1).toString()+((now.getDate() < 10 )? '0' : '')+now.getDate()+'00'+n;
    vm.title="";
    vm.dateStart="";
    vm.dateEnd="";
    vm.stateType="";
    vm.statelist=[
    	{value:1,name:'公開行程'},
    	{value:2,name:'共同編輯'},
    	{value:3,name:'只限本人'}
    ];
   // Date.parse(vm.datesort[0]) - 86400000;
    vm.a=Math.floor(Math.random() * 50)%3+1;
    vm.send=function(){
    	if(vm.title==""||vm.dateStart==""||vm.stateType==""){
    		alert('有東西沒填');
    	}else if(vm.dateStart>vm.dateEnd){
    		alert('開始日期不能大於回程日期');
    	}else{
	    	$uibModalInstance.close({
	    		uid:vm.uid,
	    		title:vm.title,
	    		dateStart:vm.dateStart,
	    		dateEnd:vm.dateEnd,
	    		stateType:vm.stateType,
	    		version:1,
	    		bgimg:'bg'+(Math.floor(Math.random() * 50)%3+1)
	    	});
	    }
    }
  });
