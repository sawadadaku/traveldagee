'use strict';

/**
 * @ngdoc function
 * @name mageetravelApp.controller:EdittravelCtrl
 * @description
 * # EdittravelCtrl
 * Controller of the mageetravelApp
 */
angular.module('mageetravelApp')
    .config(function(treeConfig) {
        treeConfig.defaultCollapsed = true; // collapse nodes by default
        treeConfig.appendChildOnHover = false; // 父節點成為子節點設置
    })
// =======^^^^^^^^ui-tree(拖曳順序)的環境設定^^^^^=======
    .controller('EdittravelCtrl', function($scope, search, storageChange) {
        var vm = this;

        vm.addapoint = "views/add/point.html";
        vm.pointinfo = "views/page/pointinfo.html";

        // =======製造假資料開始=======
        if (search.getUID().length == 0) {
            if (vm.getpro == null) {
	            vm.getpro={title:'發生了一點錯誤',dateStart:'請按【返回】重新選擇行程',dateEnd:'',version:''}
	            return
	        }
            vm.projectlist = storageChange.toGet('projectlist');
            vm.getpro = vm.projectlist[0];
        } else {
            vm.getpro = search.getUID();
        }
        vm.movetext = "";
        vm.default = [{
            "id": 1,
            "title": "",
            "timestart_hour": 8,
            "timestart_minute": 0,
            "nodes": [{
                "id": 11,
                "title": "淡水",
                "pointkind": "住",
                "stop_hour": 9,
                "stop_minute": 40
            }, {
                "id": 12,
                "title": "node1.2",
                "pointkind": "住",
            }]
        }, {
            "id": 2,
            "title": "",
            "timestart_hour": 8,
            "timestart_minute": 0,
            "nodes": [{
                "id": 21,
                "title": "二.1",
                "pointkind": "住",
            }]
        }, {
            "id": 3,
            "title": "",
            "timestart_hour": 8,
            "timestart_minute": 0,
            "nodes": [{
                "id": 31,
                "title": "三.1",
                "pointkind": "住",
            }]
        }]
        if (localStorage[vm.getpro.uid] == null) {
            var result = storageChange.toSave(vm.getpro.uid, vm.default);
        }        
        vm.daylist = storageChange.toGet(vm.getpro.uid);
        // =======製造假資料結束=======

// =======時間顯示前台處理(暫時)=======
        vm.starttime = function(data) {
            if (data == null || data == '') {
                numtext = '00';
            } else {
                var numtext = (data < 10) ? '0' + data : data;
            }
            return numtext;
        };
        vm.endtime = function(stop1,stop2,starthr,startmin) {
            var endtime = "";
            var starthour = 0;
            var stopthour = 0;
            var startminute = 0;
            var stoptminute = 0;
            if (!(starthr == null || starthr == "")) {
                starthour =  parseInt(starthr);
            }
            if (!(stop1 == null || stop1 == "")) {
                stopthour = stop1;
            }
            if (!(startmin == null || startmin == "")) {
                startminute =  parseInt(startmin);
            }
            if (!(stop2 == null || stop2 == "")) {
                stoptminute = stop2;
            }
            var minnum = startminute + startminute;
            var xhr = 0;
            if (minnum > 60) {
                xhr = minnum / 60;
                minnum = minnum % 60;
            }
            var x = '';
            var hrnum = starthour + stopthour + xhr;
            if (hrnum > 23) {
                hrnum = hrnum % 24;
                x = '隔天'
            }
            var hr = ((hrnum < 10) ? '0' + hrnum : hrnum);
            var minute = ((minnum < 10) ? '0' + minnum : minnum);
            // endtime = x + hr + ":" + minute;
            var endtime={x,hr,minute};
            return endtime;
        };


// =======增加天數=======
        vm.addday = function() {
            var nowday = vm.daylist[vm.daylist.length - 1].id;
            vm.addItem = { id: nowday + 1, title: "", timestart_hour: 8, timestart_minute: 0, nodes: [] };
            vm.daylist.push(vm.addItem);
            $('.daysbox').scrollTop($('.daysbox')[0].scrollHeight);
            storageChange.toSave(vm.getpro.uid, vm.daylist);
        }

// =======增加景點(手KEY)=======
        vm.add = [];
        vm.clearadd=function(){
        	vm.add={id: '',
                addname: '',
                addpointkind: '',
                stophr: '',
                stopmin:''};
        }
        vm.addpoint = function(data) {
            // console.log(data);
            if (data.addname == ""||data.inputday == "" || data.inputday==null) {
                alert('有東西沒填');
            }else  if(data.stophr==null){
            	alert('錯誤：\n\r超出0~23小時的時間範圍，請重新輸入停留時間！');
            }else if(data.stopmin==null){
            	alert('錯誤：\n\r超出0~59分鐘的時間範圍，請重新輸入停留時間！');
            }else {
                var targetday = data.inputday - 1;
                var newname = data.addname;
                var nowpoint = vm.daylist[targetday].nodes;
                var addItem = {
                    id: nowpoint.length * 10 + 1,
                    title: data.addname,
                    pointkind: data.addpointkind,
                    stop_hour: data.stophr,
                    stop_minute: data.stopmin
                };
                nowpoint.push(addItem);
                alert("已經在【" + "Day" + data.inputday + "】新增了1個【" + newname + "】景點");
                storageChange.toSave(vm.getpro.uid, vm.daylist);
            }
        }

// =======準備編輯景點=======
        var selectindex=[];
        vm.editpoint = function(data,mainindex,index) {
        	vm.show=1;
        	vm.modal='edit';
        	selectindex=[mainindex,index];
        	if(data.stop_hour==null){
        		data.stop_hour=0;
        	}
        	if(data.stop_minute==null){
        		data.stop_minute=0;
        	}
            vm.add = {
                id: data.id,
                addname: data.title,
                addpointkind: data.pointkind,
                stophr: data.stop_hour,
                stopmin: data.stop_minute
            };
            // console.log(selectindex);
        }


// =======儲存編輯後的景點=======
        vm.savepoint = function(data) {
            if(data.stophr==null){
            	alert('錯誤：\n\r超出0~23小時的時間範圍，請重新輸入停留時間！');
            }else if(data.stopmin==null){
            	alert('錯誤：\n\r超出0~59分鐘的時間範圍，請重新輸入停留時間！');
            }else{
            	data = {
	                id: vm.add.id,
	                title: vm.add.addname,
	                pointkind: vm.add.addpointkind,
	                stop_hour: vm.add.stophr,
	                stop_minute: vm.add.stopmin
	            }
	            var day=selectindex[0]+1;
	            var name=vm.daylist[selectindex[0]].nodes[selectindex[1]].title;
	            vm.daylist[selectindex[0]].nodes[selectindex[1]]=data;
	            alert("您對【" + "Day" + day + "】【" + name + "】完成了修改！");
            }
     		storageChange.toSave(vm.getpro.uid, vm.daylist);
        }



// =======顯示景點資訊=======
        vm.showinfo = function(data) {
            vm.infotitle = data.point.title;
        }
        vm.selected = function(num, data) {
            vm.seletdeItem = num;
            vm.seletdetitle = data.title;
        }

// =======測試更改時間(暫)=======
		vm.test=function(data){
			if(data==0){
				console.log('更改此天行程的開始時間');
			}
		}


// =======ui-tree(拖曳順序)的相關設定=======
        vm.treeOptions = {
            accept: function(sourceNodeScope, destNodesScope, destIndex) {
                // 拖曳項目移動到目的地確認是否為父節點
                var x = (sourceNodeScope.point) ? 'point' : 'day';
                // console.log(x);
                if (x == 'point') {
                    var parentnode = (destNodesScope.$parentNodesScope) ? false : true;
                    if (parentnode == true) {
                        return false;
                    }
                } else {
                    var childnode = (sourceNodeScope.isParent(destNodesScope)) ? true : false;
                    if (childnode == true) {
                        return false;
                    }
                }
                return true;
            },
            beforeDrop: function(e) {
                var moveitem = (e.source.nodeScope.point) ? '景點' : '天數';
                var originalpos = e.source.index + 1;
                var resultpos = e.dest.index + 1;

                if (moveitem == "天數") {
                    var originalday = e.source.nodeScope.index() + 1;

                    if (!(originalday == resultpos)) {
                        $("#alert").animate({ opacity: 1 });
                        vm.movetext = '原本 Day ' + originalday + '/' + '更改至 Day ' + resultpos;
                    }
                }
                if (moveitem == "景點") {
                    var itemtitle = e.source.nodeScope.$modelValue.title;
                    var originalday = e.source.nodesScope.index() + 1;
                    var resultday = e.dest.nodesScope.$parent.index() + 1;

                    if (!(originalday == resultday && originalpos == resultpos)) {
                        $("#alert").animate({ opacity: 1 });
                        vm.movetext = '【' + itemtitle + '】' + '原本 Day ' + originalday + ' 第 ' + originalpos + '個行程 /' + '更改至 Day ' + resultday + ' 第 ' + resultpos + '個行程';
                    }
                }
                $("#alert").delay(3000).animate({ opacity: 0 });
            },
            dropped: function(e) {
                var x = vm.daylist;
                storageChange.toSave(vm.getpro.uid, x);
            }
            ,
            removed: function(e) {
                var x = vm.daylist;
                storageChange.toSave(vm.getpro.uid, x);
            }
        }
    });
