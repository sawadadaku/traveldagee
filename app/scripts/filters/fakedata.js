'use strict';

/**
 * @ngdoc filter
 * @name mageetravelApp.filter:fakedata
 * @function
 * @description
 * # fakedata
 * Filter in the mageetravelApp.
 */
angular.module('mageetravelApp')
  .filter('fakedata', function () {
    return function (input,attrs) {
      var number=[];
    	for(var i = 0 ; i <attrs ;i++){
    		number.push(i);
    	}
    	// console.log(attrs)
      return number;
    };
  });
